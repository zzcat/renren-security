package io.renren.modules.news.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.service.impl.CrudServiceImpl;
import io.renren.modules.news.dao.NewsDao;
import io.renren.modules.news.dto.NewsDTO;
import io.renren.modules.news.entity.NewsEntity;
import io.renren.modules.news.service.NewsService;
import cn.hutool.core.util.StrUtil;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * 新闻管理
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2023-12-23
 */
@Service
public class NewsServiceImpl extends CrudServiceImpl<NewsDao, NewsEntity, NewsDTO> implements NewsService {

    @Override
    public QueryWrapper<NewsEntity> getWrapper(Map<String, Object> params){
        String id = (String)params.get("id");

        QueryWrapper<NewsEntity> wrapper = new QueryWrapper<>();
        wrapper.eq(StrUtil.isNotBlank(id), "id", id);

        return wrapper;
    }


}