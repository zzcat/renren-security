package io.renren.modules.news.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.alibaba.excel.annotation.write.style.ContentRowHeight;
import com.alibaba.excel.annotation.write.style.HeadRowHeight;
import lombok.Data;

import java.util.Date;

/**
 * 新闻管理
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2023-12-23
 */
@Data
public class NewsExcel {
    @ExcelProperty(value = "新闻ID")
    private Long id;
    @ExcelProperty(value = "新闻名称")
    private String name;
    @ExcelProperty(value = "图标")
    private String icon;
    @ExcelProperty(value = "排序")
    private Integer sort;
    @ExcelProperty(value = "创建时间")
    private Date createDate;

}