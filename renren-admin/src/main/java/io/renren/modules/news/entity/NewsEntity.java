package io.renren.modules.news.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

/**
 * 新闻管理
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2023-12-23
 */
@Data
@TableName("tb_news")
public class NewsEntity {

    /**
     * 新闻ID
     */
	private Long id;
    /**
     * 新闻名称
     */
	private String name;
    /**
     * 图标
     */
	private String icon;
    /**
     * 排序
     */
	private Integer sort;
    /**
     * 创建时间
     */
	private Date createDate;
}