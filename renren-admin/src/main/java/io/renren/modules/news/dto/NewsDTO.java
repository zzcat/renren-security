package io.renren.modules.news.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


/**
 * 新闻管理
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2023-12-23
 */
@Data
@ApiModel(value = "新闻管理")
public class NewsDTO implements Serializable {
    private static final long serialVersionUID = 1L;

	@ApiModelProperty(value = "新闻ID")
	private Long id;

	@ApiModelProperty(value = "新闻名称")
	private String name;

	@ApiModelProperty(value = "图标")
	private String icon;

	@ApiModelProperty(value = "排序")
	private Integer sort;

	@ApiModelProperty(value = "创建时间")
	private Date createDate;
}