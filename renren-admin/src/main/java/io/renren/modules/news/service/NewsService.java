package io.renren.modules.news.service;

import io.renren.common.service.CrudService;
import io.renren.modules.news.dto.NewsDTO;
import io.renren.modules.news.entity.NewsEntity;

/**
 * 新闻管理
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2023-12-23
 */
public interface NewsService extends CrudService<NewsEntity, NewsDTO> {

}