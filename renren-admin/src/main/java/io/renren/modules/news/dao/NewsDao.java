package io.renren.modules.news.dao;

import io.renren.common.dao.BaseDao;
import io.renren.modules.news.entity.NewsEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 新闻管理
 *
 * @author Mark sunlightcs@gmail.com
 * @since 1.0.0 2023-12-23
 */
@Mapper
public interface NewsDao extends BaseDao<NewsEntity> {
	
}