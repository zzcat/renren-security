CREATE TABLE tb_news (
    id bigint NOT NULL COMMENT '新闻ID',
    name varchar(500) COMMENT '新闻名称',
    icon varchar(500) COMMENT '图标',
    sort int COMMENT '排序',
    create_date datetime COMMENT '创建时间',
    PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='新闻管理';