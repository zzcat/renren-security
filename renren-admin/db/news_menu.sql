-- 菜单SQL
INSERT INTO `sys_menu` (`pid`, `name`, `url`, `permissions`, `menu_type`, `icon`, `sort`)
VALUES ('48', '新闻管理', 'sys/news', NULL, '1', 'larry-10109', '6');
-- 按钮父菜单ID
set @parentId = '1067246875800000035';
    -- 菜单对应按钮SQL
INSERT INTO `sys_menu` (`pid`, `name`, `url`, `permissions`, `menu_type`, `icon`, `sort`)
SELECT @parentId, '查看', null, 'sys:news:list,sys:news:info', '2', null, '6';
INSERT INTO `sys_menu` (`pid`, `name`, `url`, `permissions`, `menu_type`, `icon`, `sort`)
SELECT @parentId, '新增', null, 'sys:news:save', '2', null, '6';
INSERT INTO `sys_menu` (`pid`, `name`, `url`, `permissions`, `menu_type`, `icon`, `sort`)
SELECT @parentId, '修改', null, 'sys:news:update', '2', null, '6';
INSERT INTO `sys_menu` (`pid`, `name`, `url`, `permissions`, `menu_type`, `icon`, `sort`)
SELECT @parentId, '删除', null, 'sys:news:delete', '2', null, '6';